### Description ###

* Easy-Class is a libs for Android to make develop app much easier.
* 
* Version 1.0

### To set up Easy-Class lib ###

* Method 1. Export Easy-Class as a jar file, and put it in folder of libs in your project.
* Method 2. Import Easy-Class as a project, and right click on Project, and look for "Properties->Android->Library" to set it as library.

### Main Features ###

* FreeLayout: Solve all problems of resolution.
* EasyHttpRequest: A compact of http request method supports RESTful api.

### Contact Me ###

* Contact me with my E-mail: spring60569@gmail.com