/*
 * Copyright 2014 Thinkermobile, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.james.easyinternet;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <br>
 * Chinese Explanation: <br>
 * EasyResponseObjectParser可以用來處理一個正規命名的JSONObject字串，轉成一個object <br>
 * 需注意的是object本身的public變數必須完全對應到JSONObject的各層位置 <br>
 * <br>
 * English Explanation: <br>
 * 
 * @author JamesX
 * @since 2014_02_20
 */
public class EasyResponseObjectParser {

	private EasyResponseObjectParser() {

	}

	/**
	 * @deprecated <br>
	 *             Instead, please use <br>
	 *             <code> startParsing(String jsonString, Object target)
	 */
	public static void simpleParseObject(String jsonString, Object target) throws Exception {
		startParsing(jsonString, target);
	}

	/**
	 * <br>
	 * Chinese Explanation: <br>
	 * 用來處理JSONObject字串，object的public變數設計必須對應JSONObject的內容階層 <br>
	 * <br>
	 * English Explanation: <br>
	 * Parse a Json String into a target object. All public variables of target object must match the structure of Json String. <br>
	 * <br>
	 * Take a Json String for example: <br>
	 * 
	 * <pre>
	 * "car":{"color":"red","type":"coupe"}
	 * </pre>
	 * 
	 * <br>
	 * <br>
	 * developer must define a class like following to match the structure. <br>
	 * 
	 * <pre>
	 * public class Car {
	 * 	public String color;
	 * 	public String type;
	 * }
	 * </pre>
	 * 
	 * @param jsonString (String)
	 * @param target (Object)
	 * @throws Exception
	 */
	public static void startParsing(String jsonString, Object target) throws Exception {
		if (jsonString != null) {
			try {
				JSONObject jsonObject = new JSONObject(jsonString);
				parseJson(target, jsonObject);
			} catch (JSONException e) {
				JSONArray jsonArray = new JSONArray(jsonString);
				parseJson(target, jsonArray);
			}
		}
	}

	/**
	 * @deprecated <br>
	 *             Instead, please use <br>
	 *             <code> startParsing(EasyResponseObject easyResponseObject, Object target)
	 */
	public static void simpleParseObject(EasyResponseObject easyResponseObject, Object target) throws Exception {
		startParsing(easyResponseObject, target);
	}

	/**
	 * <br>
	 * Chinese Explanation: <br>
	 * 用來處理JSONObject字串，object的public變數設計必須對應JSONObject的內容階層 <br>
	 * <br>
	 * English Explanation: <br>
	 * Parse a Json String into a target object. All public variables of target object must match the structure of Json String. <br>
	 * Json String here denotes the body of easyResponseObject. <br>
	 * <br>
	 * Take a Json String for example: <br>
	 * 
	 * <pre>
	 * "car":{"color":"red","type":"coupe"}
	 * </pre>
	 * 
	 * <br>
	 * <br>
	 * developer must define a class like following to match the structure. <br>
	 * 
	 * <pre>
	 * public class Car {
	 * 	public String color;
	 * 	public String type;
	 * }
	 * </pre>
	 * 
	 * @param easyResponseObject (EasyResponseObject)
	 * @param target (Object)
	 * @throws Exception
	 */
	public static void startParsing(EasyResponseObject easyResponseObject, Object target) throws Exception {
		if (easyResponseObject != null && easyResponseObject.getBody() != null) {
			JSONObject jsonObject = new JSONObject(easyResponseObject.getBody());
			parseJson(target, jsonObject);
		}
	}

	private static void parseJson(Object target, Object jsonObjectOrArray) throws JSONException, IllegalArgumentException, IllegalAccessException, ArrayIndexOutOfBoundsException, InstantiationException {
		Field[] fields = target.getClass().getFields();
		for (int i = 0; i < fields.length; i++) {
			//
			String name = fields[i].getName();

			if (jsonObjectOrArray instanceof JSONObject && !((JSONObject) jsonObjectOrArray).has(name)) {
				continue;
			}

			final Object object;
			if (jsonObjectOrArray instanceof JSONObject) {
				object = ((JSONObject) jsonObjectOrArray).get(name);
			}
			else {
				object = jsonObjectOrArray;
			}

			if (object instanceof JSONArray) {
				Class<?> componentType = fields[i].getType().getComponentType();

				JSONArray jsonArray = (JSONArray) object;
				fields[i].set(target, Array.newInstance(componentType, jsonArray.length()));

				for (int index = 0; index < jsonArray.length(); index++) {
					Object object2 = jsonArray.get(index);
					if (object2 instanceof JSONObject) {
						Array.set(fields[i].get(target), index, componentType.newInstance());
						parseJson(Array.get(fields[i].get(target), index), (JSONObject) object2);
					}
					else if (object2 instanceof String) {
						Array.set(fields[i].get(target), index, (String) object2);
					}
					else if (object2 instanceof Integer || object2 instanceof Long) {
						if (fields[i].get(target).getClass().getFields()[0].getType().getSimpleName().toLowerCase().contains("int")) {
							Array.set(fields[i].get(target), index, Integer.parseInt(object2.toString()));
						}
						else if (fields[i].get(target).getClass().getFields()[0].getType().getSimpleName().toLowerCase().contains("long")) {
							Array.set(fields[i].get(target), index, Long.parseLong(object2.toString()));
						}
					}
					else if (object2 instanceof Float || object2 instanceof Double) {
						if (fields[i].get(target).getClass().getFields()[0].getType().getSimpleName().toLowerCase().contains("float")) {
							Array.set(fields[i].get(target), index, Float.parseFloat(object2.toString()));
						}
						else if (fields[i].get(target).getClass().getFields()[0].getType().getSimpleName().toLowerCase().contains("double")) {
							Array.set(fields[i].get(target), index, Double.parseDouble(object2.toString()));
						}
					}
					else if (object2 instanceof Boolean) {
						Array.set(fields[i].get(target), index, (Boolean) object2);
					}
				}
			}
			else if (object instanceof JSONObject) {
				fields[i].set(target, fields[i].getType().newInstance());
				parseJson(fields[i].get(target), (JSONObject) object);
			}
			else if (object instanceof String) {
				fields[i].set(target, (String) object);
			}
			else if (object instanceof Integer || object instanceof Long) {
				if (fields[i].getType().getSimpleName().toLowerCase().contains("int")) {
					fields[i].set(target, Integer.parseInt(object.toString()));
				}
				else if (fields[i].getType().getSimpleName().toLowerCase().contains("long")) {
					fields[i].set(target, Long.parseLong(object.toString()));
				}
			}
			else if (object instanceof Float || object instanceof Double) {
				if (fields[i].getType().getSimpleName().toLowerCase().contains("float")) {
					fields[i].set(target, Float.parseFloat(object.toString()));
				}
				else if (fields[i].getType().getSimpleName().toLowerCase().contains("double")) {
					fields[i].set(target, Double.parseDouble(object.toString()));
				}
			}
			else if (object instanceof Boolean) {
				fields[i].set(target, (Boolean) object);
			}
		}
	}
}
