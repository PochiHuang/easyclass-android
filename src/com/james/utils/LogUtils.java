/*
 * Copyright 2012 Thinkermobile, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.james.utils;

import android.util.Log;

/**
 * 
 * @author JamesX
 * @since 2012/10/15
 */
public class LogUtils {
	
	private static boolean show = true;
	
	public static void enable(){
		show = true;
	}
	
	public static void disable(){
		show = false;
	}
	
	public static void v(String tag, String msg){
		if(show){
			Log.v(tag, msg);
		}
	}
	
	public static void w(String tag, String msg){
		if(show){
			Log.w(tag, msg);
		}
	}
	
	public static void e(String tag, String msg){
		if(show){
			Log.e(tag, msg);
		}
	}
	
	public static void i(String tag, String msg){
		if(show){
			Log.i(tag, msg);
		}
	}
	
	public static void d(String tag, String msg){
		if(show){
			Log.d(tag, msg);
		}
	}
}
